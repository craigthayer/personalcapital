# personalcapital

Node.js library for interacting with the PersonalCapital API.

Inspired by:
* https://github.com/haochi/personalcapital
* https://github.com/JamaicanMoose/personalcapital-js

## Installation

```bash
npm install --save personalcapital
```

## Authentication Example

```js
const { PersonalCapital, MFARequiredError } = require('personalcapital');
const readline = require('readline-sync');

let pc = new PersonalCapital();

let username = readline.question('Email: ');
let password = readline.question('Password: ');

try {
    await pc.login(username, password);
} catch (e) {
    if (!(e instanceof MFARequiredError)) {
      throw e;
    }
    
    await pc.twoFactorChallenge(PersonalCapital.MFA_VERIFY_MODE.SMS);
    await pc.twoFactorAuthenicate(PersonalCapital.MFA_VERIFY_MODE.SMS, readline.question('Enter MFA Code: '));
    
    let resp = await pc.authenticatePassword(password);

    console.log(resp);
}
```

Once the `cookies.json` file is stored in the `cookiePath` location, you can login without MFA:

```js
const { PersonalCapital } = require('personalcapital');
const readline = require('readline-sync');

let pc = new PersonalCapital();

let username = readline.question('Email: ');
let password = readline.question('Password: ');

try {
    await pc.login(username, password);
} catch (e) {
  console.log('login failed');
  throw e;
}

console.log(await pc.getAccounts());
```

***Note:** When starting a session from saved cookies, you MUST call `login()` before you can use the other functions that interact with the API.*

## Options

The `PersonalCapital` class allows the following options to be set via its constructor function:

`cookiePath`: The path (excluding the filename) to the json file that stores the session data for the PersonalCapital API.

```js
let pc = new PersonalCapital({cookiePath: '/some/path'});
```

## Methods

* `login()`
* `getAccounts()`
* `getCashFlow()`
* `getTransactions()`
* `getHoldings()`
* `getHistories()`
* `twoFactorChallenge()`
* `twoFactorAuthenicate()`
* `authenticatePassword()`


## Tests

To test, you must have a PersonalCapital account and create a json file at `test/testConfig.json` with the following contents:

```json
{
  "personalCapital": {
    "username": "your email address",
    "password": "your password"
  }
}

```
