'use strict';

const request = require('request');
const FileCookieStore = require('tough-cookie-filestore');
const path = require('path');
const _ = require('lodash');
const MFARequiredError = require('./mfaRequiredError');
const fs = require('fs');

const BASE_URL = 'https://home.personalcapital.com';
const API_ENDPOINT = `${BASE_URL}/api`;

const SP_HEADER_KEY = 'spHeader';
const SUCCESS_KEY = 'success';
const CSRF_KEY = 'csrf';
const AUTH_LEVEL_KEY = 'authLevel';
const ERRORS_KEY = 'errors';

const MFA_VERIFY_MODE = Object.freeze({
  SMS: 0,
  PHONE: 1,
  EMAIL: 2
});

const AUTH_LEVEL = Object.freeze({
  USER_REMEMBERED: 'USER_REMEMBERED'
});

const API_ENDPOINTS = Object.freeze({
  //Auth
  identifyUser: '/login/identifyUser',
  querySession: '/login/querySession',
  //Auth
  challengeSms: '/credential/challengeSms',
  authenticateSms: '/credential/authenticateSms',
  challengeEmail: '/credential/challengeEmail',
  authenticateEmail: '/credential/authenticateEmail',
  authenticatePassword: '/credential/authenticatePassword',
  //DeAuth
  switchUser: '/login/switchUser',
  //Fetch
  getCategories: '/transactioncategory/getCategories',
  getUserMessages: '/message/getUserMessages',
  getAccounts: '/newaccount/getAccounts2',
  // getAccounts: '/newaccount/getAccounts',
  getAdvisor: '/profile/getAdvisor',
  getFunnelAttributes: '/profile/getFunnelAttributes',
  getPerson: '/person/getPerson',
  getHistories: '/account/getHistories',
  getUserSpending: '/account/getUserSpending',
  getRetirementCashFlow: '/account/getRetirementCashFlow',
  getQuotes: '/invest/getQuotes',
  getHoldings: '/invest/getHoldings',
  searchSecurity: '/invest/searchSecurity',
  getUserTransactions: '/transaction/getUserTransactions',
  getCustomProducts: '/search/getCustomProducts',
  //Push
  updateUserMessages: '/message/updateUserMessages',
  createAccounts2: '/newaccount/createAccounts2'
});

const defaultOptions = Object.freeze({
  cookiePath: path.resolve(process.cwd())
});

function getSpHeaderValue(result, valueKey) {
  try {
    if (result[SP_HEADER_KEY] && result[SP_HEADER_KEY][valueKey]) {
      return result[SP_HEADER_KEY][valueKey];
    }
  } catch (e) {
    return null;
  }

  return null;
}

function getErrorValue(result) {
  try {
    return getSpHeaderValue(result, ERRORS_KEY)[0]['message'];
  } catch (e) {
    return null;
  }
}

class PersonalCapital {
  constructor(options) {
    if (!_.isPlainObject(options)) {
      options = {};
    }

    this._options = _.merge({}, defaultOptions, options);

    this._options.cookieFile = path.join(this._options.cookiePath, 'cookies.json');

    if (!fs.existsSync(this._options.cookieFile)) {
      fs.writeFileSync(this._options.cookieFile, '');
    }

    this._csrf = '';
    this._session = request.jar(new FileCookieStore(this._options.cookieFile));
  }

  async authenticatePassword(password) {
    return await this._authenticatePassword(password)
  }

  async fetch (endpoint, data = null) {
    let payload = {
      lastServerChangeId: -1,
      csrf: this._csrf,
      apiClient: 'WEB'
    };
    
    if (data) {
      _.merge(payload, data);
    }
    
    return await this.post(endpoint, payload);
  }

  async login(username, password) {
    //let initialCsrf = this._getCsrfFromHomePage(BASE_URL);
    let userData = await this._identifyUser(username, null);

    if (_.isEmpty(userData) || _.isUndefined(userData.csrf) || _.isUndefined(userData.authLevel)) {
      throw new Error('login failed');
    }

    this._csrf = userData.csrf;

    if (userData.authLevel !== AUTH_LEVEL.USER_REMEMBERED) {
      throw new MFARequiredError('2FA required');
    }

    let result = await this._authenticatePassword(password);

    try {
      result = JSON.parse(result);
    } catch (e) {}

    if (getSpHeaderValue(result.body, SUCCESS_KEY) === null) {
      throw new Error(getErrorValue(result));
    }
  }

  post(endpoint, data) {
    return new Promise((accept, reject) => {
      let req = request.post({url: `${API_ENDPOINT}${endpoint}`, form: data, jar: this._session}, (err, resp, body) => {
        if (err) {
          reject(err);
          return;
        }

        if (_.isString(body)) {
          resp.body = JSON.parse(body);
        }

        accept(resp);
      });
    });
  }

  async twoFactorAuthenicate(mode, code) {
    let authFunction = async () => { return null; };

    switch (mode) {
      case MFA_VERIFY_MODE.SMS:
        authFunction = this._authenticateSMS.bind(this);
        break;

      case MFA_VERIFY_MODE.EMAIL:
        authFunction = this._authenticateEmail.bind(this);
        break;

      default:
        break;
    }

    return await authFunction(code);
  }

  async twoFactorChallenge(mode) {
    let challengeFunction = async () => { return null; };
    
    switch (mode) {
      case MFA_VERIFY_MODE.SMS:
        challengeFunction = this._challengeSMS.bind(this);
        break;
        
      case MFA_VERIFY_MODE.EMAIL:
        challengeFunction = this._challengeEmail.bind(this);
        break;
        
      default:
        break;
    }
    
    return await challengeFunction();
  }

  /*
   * Convenience Methods
   */

  async getAccounts() {
    return await this.endpoint(API_ENDPOINTS.getAccounts);
  }

  async getCashFlow(accounts, startDate, endDate, interval) {
    return await this.endpoint(API_ENDPOINTS.getHistories, this._generateHistoriesPayload(accounts, startDate, endDate, interval, ['cashflows']));
  }

  async getTransactions(accounts, startDate, endDate) {
    return await this.endpoint(API_ENDPOINTS.getUserTransactions, this._generateTransactionsPayload(accounts, startDate, endDate));
  }

  async getHoldings(accounts) {
    return await this.endpoint(API_ENDPOINTS.getHoldings, this._generateHoldingsPayload(accounts));
  }

  async getHistories(accounts, startDate, endDate, interval, types) {
    return await this.endpoint(API_ENDPOINTS.getHistories, this._generateHistoriesPayload(accounts, startDate, endDate, interval, types));
  }

  async endpoint(url, data = null) {
    let resp = await this.fetch(url, data);

    return this._parseResponseBody(resp.body);
  }

  /*
   * Private Methods
   */

  async _authenticateEmail(code) {
    let data = this._generateAuthenticationPayload(code);
    return await this.post(API_ENDPOINTS.authenticateEmail, data);
  }

  async _authenticatePassword(password) {
    let data = {
      bindDevice: 'true',
      deviceName: '',
      redirectTo: '',
      skipFirstUse: '',
      skipLinkAccount: 'false',
      referrerId: '',
      passwd: password,
      apiClient: 'WEB',
      csrf: this._csrf
    };

    return await this.post(API_ENDPOINTS.authenticatePassword, data);
  }

  async _authenticateSMS(code) {
    let data = this._generateAuthenticationPayload(code);
    return await this.post(API_ENDPOINTS.authenticateSms, data);
  }

  async _challengeEmail() {
    let data = this._generateChallengePayload('challengeEmail');
    return await this.post(API_ENDPOINTS.challengeEmail, data);
  }

  async _challengeSMS() {
    let data = this._generateChallengePayload('challengeSMS');
    return await this.post(API_ENDPOINTS.challengeSms, data);
  }

  _generateAuthenticationPayload(code) {
    return {
      challengeReason: 'DEVICE_AUTH',
      challengeMethod: 'OP',
      apiClient: 'WEB',
      bindDevice: 'false',
      code: code,
      csrf: this._csrf
    };
  }

  _generateChallengePayload(challengeType) {
    return {
      challengeReason: 'DEVICE_AUTH',
      challengeMethod: 'OP',
      challengeType: challengeType,
      apiClient: 'WEB',
      bindDevice: 'false',
      csrf: this._csrf
    };
  }

  _generateTransactionsPayload(accounts, startDate, endDate) {
    return {
      userAccountIds: accounts,
      startDate,
      endDate,
      page: 0,
      rows: -1,
      sort_cols: 'transactionTime',
      component: 'DATAGRID'
    };
  }

  _generateHoldingsPayload(accounts) {
    return {
      userAccountIds: accounts
    };
  }

  _generateHistoriesPayload(accounts, startDate, endDate, interval, types) {
    return {
      userAccountIds: accounts,
      startDate,
      endDate,
      intervalType: interval,
      types
    };
  }

  // _getCsrfFromHomePage(url) {
  //   let cookies = this._session.getCookies(url);
  //
  //   if (_.isUndefined(cookies, CSRF_COOKIE_NAME)) {
  //     return null;
  //   }
  //
  //   return cookies[CSRF_COOKIE_NAME];
  // }

  async _identifyUser(username, csrf) {
    let data = {
      username: username,
      csrf: csrf,
      apiClient: 'WEB',
      bindDevice: 'false',
      skipLinkAccount: 'false',
      redirectTo: '',
      skipFirstUse: '',
      referrerId: '',
    };
    
    let resp = await this.post(API_ENDPOINTS.identifyUser, data);

    let res = resp.body;

    if (_.isString(res)) {
      res = JSON.parse(res);
    }

    let newCSRF = getSpHeaderValue(res, CSRF_KEY);
    let authLevel = getSpHeaderValue(res, AUTH_LEVEL_KEY);

    return {
      csrf: newCSRF,
      authLevel: authLevel
    };
  }

  _parseResponseBody(body) {
    return body.spData;
  }
}

PersonalCapital.MFA_VERIFY_MODE = MFA_VERIFY_MODE;
PersonalCapital.AUTH_LEVEL = AUTH_LEVEL;
PersonalCapital.API_ENDPOINTS = API_ENDPOINTS;

module.exports = PersonalCapital;
