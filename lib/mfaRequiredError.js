'use strict';

class MFARequiredError extends Error {
  constructor(message = '') {
    super(message);
  }
}

module.exports = MFARequiredError;
