'use strict';

const assert = require('assert');
const { PersonalCapital, MFARequiredError } = require(`${__dirname}/../.`);
const fs = require('fs');
const readline = require('readline-sync');
const _ = require('lodash');

const testConfig = require(`${__dirname}/testConfig`);

describe('MFARequiredError', function () {
  let error = new MFARequiredError('test');

  it('should be an instance of MFARequiredError', function () {
    assert.equal(error instanceof MFARequiredError, true);
  });

  it('should be instance of Error', () => {
    assert.equal(error instanceof Error, true);
  });
});

describe('PersonalCapital', function () {
  let pc = new PersonalCapital({cookiePath: __dirname});

  after(function (done) {
    fs.exists(pc._options.cookieFile, (exists) => {
      if (!exists) {
        return;
      }

      fs.unlink(pc._options.cookieFile, done);
    });
  });

  it('should be an instance of PersonalCapital', function () {
    assert.equal(pc instanceof PersonalCapital, true);
  });

  it('should create the cookieFile', function (done) {
    fs.exists(pc._options.cookieFile, (exists) => {
      assert.equal(exists, true);
      done();
    });
  });

  describe('#login()', function () {
    it('should throw MFARequiredError', async function () {
      try {
        await pc.login(testConfig.personalCapital.username, testConfig.personalCapital.password);
        assert.equal(true, false, '#login() did not throw an exception');
      } catch (e) {
        assert.equal(e instanceof MFARequiredError, true);
      }
    });

    it('should authenticate with mfa', async function () {
      this.timeout(30000);

      try {
        await pc.login(testConfig.personalCapital.username, testConfig.personalCapital.password);
        assert.equal(true, false, '#login() did not through an exception');
      } catch (e) {
        assert.equal(e instanceof MFARequiredError, true);

        await pc.twoFactorChallenge(PersonalCapital.MFA_VERIFY_MODE.SMS);
        await pc.twoFactorAuthenicate(PersonalCapital.MFA_VERIFY_MODE.SMS, readline.question('Enter MFA Code: '));

        let resp = await pc.authenticatePassword(testConfig.personalCapital.password);

        assert.equal(_.isUndefined(resp.body.spHeader), false);
        assert.equal(resp.body.spHeader.success, true);
      }
    });

    it('should login using cookie (no MFA)', async function () {
      try {
        await pc.login(testConfig.personalCapital.username, testConfig.personalCapital.password);
        assert.equal(true, true, '#login() did not throw an exceptions');
      } catch (e) {
        assert.equal(e instanceof MFARequiredError, true);
        assert.equal(true, false, '#login() threw an exception');
      }
    });
  });

  describe('#fetch()', function () {
    it('should get accounts', async function () {
      let resp = await pc.fetch('/newaccount/getAccounts');

      assert.equal(resp.body.spHeader.success, true);
      assert.equal(_.isArray(resp.body.spData.accounts), true);
    });
  });

  describe('#getAccounts', function () {
    it('should get accounts', async function () {
      let data = await pc.getAccounts();

      assert.equal(_.isArray(data.accounts), true);
    });
  });
});
