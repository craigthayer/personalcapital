'use strict';

module.exports = {
  MFARequiredError: require('./lib/mfaRequiredError'),
  PersonalCapital: require('./lib/personalCapital')
};
